package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    @Test
    void testConstructor() {
        Item expectedItem = new StandardItem(1, "name", 1.0f, "category", 1);
        int expectedCount = 0;

        ItemStock itemStock = new ItemStock(expectedItem);

        assertEquals(expectedItem, itemStock.getItem());
        assertEquals(expectedCount, itemStock.getCount());
    }

    static Collection<Arguments> provideItemsForIncreaseAndDecreaseItemCountTest() {
        return List.of(
            Arguments.of(1),
            Arguments.of(0),
            Arguments.of(-3),
            Arguments.of(6)
        );
    }

    @ParameterizedTest
    @MethodSource("provideItemsForIncreaseAndDecreaseItemCountTest")
    void increaseItemCountTest(int n) {
        ItemStock itemStock = new ItemStock(
            new StandardItem(1, "name", 1.0f, "category", 1)
        );
        int initialCount = itemStock.getCount();

        itemStock.increaseItemCount(n);

        assertEquals(initialCount + n, itemStock.getCount());
    }

    @ParameterizedTest
    @MethodSource("provideItemsForIncreaseAndDecreaseItemCountTest")
    void decreaseItemCountTest(int n) {
        ItemStock itemStock = new ItemStock(
            new StandardItem(1, "name", 1.0f, "category", 1)
        );
        int initialCount = itemStock.getCount();

        itemStock.decreaseItemCount(n);

        assertEquals(initialCount - n, itemStock.getCount());
    }
}