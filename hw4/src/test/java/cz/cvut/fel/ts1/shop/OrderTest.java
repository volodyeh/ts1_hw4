package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void testConstructor1() {
        ShoppingCart expectedCart = new ShoppingCart();
        String expectedCustomerName = "name";
        String expectedCustomerAddress = "address";
        int expectedState = 1;

        Order order = new Order(expectedCart, expectedCustomerName, expectedCustomerAddress, expectedState);

        assertEquals(expectedCart.getCartItems(), order.getItems());
        assertEquals(expectedCustomerName, order.getCustomerName());
        assertEquals(expectedCustomerAddress, order.getCustomerAddress());
        assertEquals(expectedState, order.getState());
    }

    @Test
    void testConstructorForNullCart() {
        assertThrows(
            NullPointerException.class,
            () -> new Order(null, "name", "address", 1)
        );
    }

}