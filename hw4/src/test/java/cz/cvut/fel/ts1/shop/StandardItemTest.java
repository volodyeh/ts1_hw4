package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    @Test
    void testConstructor() {
        int expectedId = 1;
        String expectedName = "name";
        float expectedPrice = 1.0f;
        String expectedCategory = "category";
        int expectedLoyaltyPoints = 1;

        StandardItem item = new StandardItem(
            expectedId, expectedName, expectedPrice, expectedCategory, expectedLoyaltyPoints
        );

        assertEquals(expectedId, item.getID());
        assertEquals(expectedName, item.getName());
        assertEquals(expectedPrice, item.getPrice());
        assertEquals(expectedCategory, item.getCategory());
        assertEquals(expectedLoyaltyPoints, item.getLoyaltyPoints());
    }

    @Test
    void testCopy() {
        int expectedId = 1;
        String expectedName = "name";
        float expectedPrice = 1.0f;
        String expectedCategory = "category";
        int expectedLoyaltyPoints = 1;

        StandardItem item = new StandardItem(
            expectedId, expectedName, expectedPrice, expectedCategory, expectedLoyaltyPoints
        );

        StandardItem itemCopy = item.copy();

        assertEquals(item.getID(), itemCopy.getID());
        assertEquals(item.getName(), itemCopy.getName());
        assertEquals(item.getPrice(), itemCopy.getPrice());
        assertEquals(item.getCategory(), itemCopy.getCategory());
        assertEquals(item.getLoyaltyPoints(), itemCopy.getLoyaltyPoints());
    }

    static Collection<Arguments> provideItemsForEqualsPositiveTest() {
        return List.of(
            Arguments.of(
                new StandardItem(2, "name2", 1.0f, "category2", 1),
                new StandardItem(2, "name2", 1.0f, "category2", 1)
            ),
            Arguments.of(
                new StandardItem(3, "name3", 1.0f, "category2", 1),
                new StandardItem(3, "name3", 1.0f, "category2", 1)
            )
        );
    }

    static Collection<Arguments> provideItemsForEqualsNegativeTest() {
        return List.of(
            Arguments.of(
                new StandardItem(1, "name1", 1.0f, "category1", 1),
                new StandardItem(2, "name2", 1.0f, "category2", 1)
            ),
            Arguments.of(
                new StandardItem(2, "name2", 1.0f, "category2", 4),
                new StandardItem(2, "name2", 1.0f, "category2", 1)
            )
        );
    }

    @ParameterizedTest
    @MethodSource("provideItemsForEqualsPositiveTest")
    void positiveTestEquals(StandardItem item1, StandardItem item2) {
        assertTrue(item1.equals(item2));
    }

    @ParameterizedTest
    @MethodSource("provideItemsForEqualsNegativeTest")
    void negativeTestEquals(StandardItem item1, StandardItem item2) {
        assertFalse(item1.equals(item2));
    }
}