package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.storage.NoItemInStorage;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class EShopControllerTest {

    @Test
    void testStartEShop() throws IllegalAccessException, NoSuchFieldException {
        EShopController eShopController = new EShopController();
        EShopController.startEShop();

        Field storageField = EShopController.class.getDeclaredField("storage");
        Field archiveField = EShopController.class.getDeclaredField("archive");
        Field cartsField = EShopController.class.getDeclaredField("carts");
        Field ordersField = EShopController.class.getDeclaredField("orders");
        storageField.setAccessible(true);
        archiveField.setAccessible(true);
        cartsField.setAccessible(true);
        ordersField.setAccessible(true);

        assertNotNull(storageField.get(eShopController));
        assertNotNull(archiveField.get(eShopController));
        assertNotNull(cartsField.get(eShopController));
        assertNotNull(ordersField.get(eShopController));
    }

    @Test
    void shoppingProcessPositiveTest() throws NoItemInStorage {
        EShopController.startEShop();

        int[] itemCount = {10,10,4,5,10,2};

        Item[] storageItems = {
            new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
            new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
            new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
            new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
            new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
            new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        for (int i = 0; i < storageItems.length; i++) {
            EShopController.getStorage().insertItems(storageItems[i], itemCount[i]);
        }

        ShoppingCart cart = EShopController.newCart();
        cart.addItem(storageItems[0]);
        cart.addItem(storageItems[1]);
        cart.addItem(storageItems[2]);
        cart.addItem(storageItems[3]);
        cart.addItem(storageItems[4]);

        cart.removeItem(storageItems[0].getID());

        cart.addItem(storageItems[5]);

        EShopController.purchaseShoppingCart(cart, "test name", "test address");

        assertEquals(1, EShopController.getArchive().getOrders().size());
    }

    @Test
    void shoppingProcessNegativeTest() {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();
        cart.addItem(new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5));

        assertThrows(
            NoItemInStorage.class,
            () -> EShopController.purchaseShoppingCart(cart, "test name", "test address")
        );
    }

}