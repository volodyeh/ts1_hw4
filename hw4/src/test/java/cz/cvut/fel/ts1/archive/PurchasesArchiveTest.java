package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class PurchasesArchiveTest {

    private static HashMap<Integer, ItemPurchaseArchiveEntry> getItemPurchaseArchiveMap(
        List<ItemPurchaseArchiveEntry> itemPurchaseArchiveEntries
    ) {
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();

        for (ItemPurchaseArchiveEntry entry : itemPurchaseArchiveEntries) {
            itemPurchaseArchive.put(entry.getRefItem().getID(), entry);
        }

        return itemPurchaseArchive;
    }

    private static ItemPurchaseArchiveEntry getItemPurchaseArchiveEntry(StandardItem item, int soldCount) {
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);
        itemPurchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(-1);
        itemPurchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(soldCount);

        return itemPurchaseArchiveEntry;
    }

    static Collection<Arguments> provideItemPurchaseArchivesAndExpectedStatisticsOutput() {
        return List.of(
            Arguments.of(
                getItemPurchaseArchiveMap(List.of(
                    new ItemPurchaseArchiveEntry(
                        new StandardItem(1, "name1", 1.0f, "category1", 1)
                    ),
                    new ItemPurchaseArchiveEntry(
                        new StandardItem(2, "name2", 2.0f, "category2", 2)
                    ),
                    new ItemPurchaseArchiveEntry(
                        new StandardItem(3, "name3", 3.0f, "category3", 3)
                    )
                )),
                """
                ITEM PURCHASE STATISTICS:
                ITEM  Item   ID 1   NAME name1   CATEGORY category1   PRICE 1.0   LOYALTY POINTS 1   HAS BEEN SOLD 1 TIMES
                ITEM  Item   ID 2   NAME name2   CATEGORY category2   PRICE 2.0   LOYALTY POINTS 2   HAS BEEN SOLD 1 TIMES
                ITEM  Item   ID 3   NAME name3   CATEGORY category3   PRICE 3.0   LOYALTY POINTS 3   HAS BEEN SOLD 1 TIMES
                """
            ),
            Arguments.of(
                getItemPurchaseArchiveMap(List.of()),
                """
                ITEM PURCHASE STATISTICS:
                """
            )
        );
    }

    static Collection<Arguments> provideItemPurchaseArchivesAndExpectedItemSoldOutput() {
        return List.of(
            Arguments.of(
                getItemPurchaseArchiveEntry(
                    new StandardItem(1, "name1", 1.0f, "category1", 1),
                    5
                ), 5
            ),
            Arguments.of(
                getItemPurchaseArchiveEntry(
                    new StandardItem(1, "name1", 1.0f, "category1", 1),
                    0
                ), 0
            )
        );
    }

    @SuppressWarnings("unchecked")
    private final ArrayList<Order> mockOrderArchive = mock(ArrayList.class);

    @ParameterizedTest
    @MethodSource("provideItemPurchaseArchivesAndExpectedStatisticsOutput")
    void printItemPurchaseStatisticsTest(
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive,
        String expectedOutput
    ) {
        PurchasesArchive purchasesArchive = new PurchasesArchive(
            itemArchive, mockOrderArchive
        );

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        purchasesArchive.printItemPurchaseStatistics();

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        assertEquals(expectedOutput, outContent.toString());
    }

    @ParameterizedTest
    @MethodSource("provideItemPurchaseArchivesAndExpectedItemSoldOutput")
    void getHowManyTimesHasBeenItemSoldTest(
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry,
        int expectedItemSoldCount
    ) {
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = getItemPurchaseArchiveMap(List.of(
            itemPurchaseArchiveEntry
        ));
        PurchasesArchive purchasesArchive = new PurchasesArchive(
            itemArchive, mockOrderArchive
        );

        assertEquals(
            expectedItemSoldCount,
            purchasesArchive.getHowManyTimesHasBeenItemSold(itemArchive.get(1).getRefItem())
        );
    }

    @Test
    void putOrderToPurchasesArchiveTest() {
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        ArrayList<Item> items = new ArrayList<>();
        Item item1 = new StandardItem(1, "name1", 1.0f, "category1", 1);
        Item item2 = new StandardItem(2, "name2", 2.0f, "category2", 2);
        Item item3 = new StandardItem(3, "name3", 3.0f, "category3", 3);

        items.add(item1);
        purchasesArchive.putOrderToPurchasesArchive(
            new Order(new ShoppingCart(items), "name", "address")
        );
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item1));

        purchasesArchive.putOrderToPurchasesArchive(
            new Order(new ShoppingCart(items), "name", "address")
        );
        assertEquals(2, purchasesArchive.getHowManyTimesHasBeenItemSold(item1));

        items.add(item2);
        purchasesArchive.putOrderToPurchasesArchive(
            new Order(new ShoppingCart(items), "name", "address")
        );
        assertEquals(3, purchasesArchive.getHowManyTimesHasBeenItemSold(item1));
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item2));

        items.clear();
        items.add(item3);
        purchasesArchive.putOrderToPurchasesArchive(
            new Order(new ShoppingCart(items), "name", "address")
        );
        assertEquals(3, purchasesArchive.getHowManyTimesHasBeenItemSold(item1));
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item2));
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item3));
    }
}