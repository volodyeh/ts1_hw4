package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemPurchaseArchiveEntryTest {

    @Test
    void testConstructor() {
        Item item = new StandardItem(1, "name1", 1.0f, "category1", 1);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);

        assertEquals(item, itemPurchaseArchiveEntry.getRefItem());
        assertEquals(1, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
    }

    @Test
    void testConstructorNull() {
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(null);

        assertEquals(null, itemPurchaseArchiveEntry.getRefItem());
        assertEquals(1, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
    }

}